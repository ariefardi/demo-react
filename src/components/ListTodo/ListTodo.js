import PropTypes from 'prop-types'; // ES6


// stateless component
import ListItem from "../ListItem/ListItem";
const ListTodo = (props) => {
  const { todos, title, deleteTodo, setUpdateData } = props;
  return (
    <div>
      <h1> {title} </h1>
      <ul>
        {todos.map((todo, index) => (
          <ListItem
            deleteTodo={deleteTodo}
            todo={todo}
            key={todo.id}
            index={String(index)}
            setUpdateData={setUpdateData}
          />
        ))}
      </ul>
    </div>
    )
}


ListTodo.propTypes = {
  deleteTodo: PropTypes.func.isRequired,
  setUpdateData: PropTypes.func.isRequired,
  title: PropTypes.string,
  todos: PropTypes.array.isRequired
}

ListTodo.defaultProps =  {
  deleteTodo: () => alert('you need to set the properties function of deleteTodo'),
  setUpdateData: () => alert('you need to set the properties function of setUpdateData'),
  title: 'No Title yet',
  todos: []
}
export default ListTodo;