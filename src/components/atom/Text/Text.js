import PropTypes from 'prop-types'; // ES6

const styles = {
  textStyles: () => ({
    fontSize: 14,
    color: 'black',
    fontWeight: 400
  })
}

const Text = ({ value, customStyle }) => {
  return (
    <div
      style={{
        ...styles.textStyles(),
        ...customStyle
      }}
    >
      {value}
    </div>
  )
}
Text.propTypes = {
  value: PropTypes.string,
  customStyle: PropTypes.object
}

Text.defaultProps = {
  value: 'no text provided',
  customStyle: {}
}
export default Text;