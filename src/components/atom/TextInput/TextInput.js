import PropTypes from 'prop-types'; // ES6
import styles from './TextInput.module.css';
const TextInput = ({ value, type, placeholder, onChange, onKeyPress, disabled }) => {
    return (
        <div>
            <input
              className={[styles['text-input']]}
              value={value}
              type={type}
              placeholder={placeholder}
              onChange={onChange}
              onKeyPress={onKeyPress}
              disabled={disabled}
            />
        </div>
    )
}


TextInput.propTypes = {
  value: PropTypes.string,
  type: PropTypes.string,
  placeholder: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  onKeyPress: PropTypes.func,
  disabled: PropTypes.bool
}

TextInput.defaultProps = {
  type: 'text',
  placeholder: 'placeholder here...',
  onChange: () => null,
  onKeyPress: () => null,
  disabled: false
}
export default TextInput;