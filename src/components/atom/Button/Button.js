import PropTypes from 'prop-types'; // ES6
import styles from './Button.module.css';

const Button = ({ title, onClick, textColor, customStyle, disabled }) => {
  return (
    <button
      disabled={disabled}
      className={styles.button}
      style={{
        ...customStyle,
        color: textColor
      }}
      onClick={onClick}
    >
      { title }
    </button>
  )
}


Button.propTypes = {
  title: PropTypes.string,
  onClick: PropTypes.func,
  textColor: PropTypes.string,
  customStyle: PropTypes.object,
  disabled: PropTypes.bool
}

Button.propTypes = {
  title: 'Submit',
  onClick: () => null,
  textColor: 'white',
  customStyle: {},
  disabled: false
}
export default Button;