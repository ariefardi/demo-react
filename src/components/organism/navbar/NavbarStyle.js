export default {
  navbar: ({ backgroundColor, height }) => {
    return {
      backgroundColor: `#${backgroundColor}`,
      width: '100%',
      height: height
    }
  }
}