// import './Navbar.module.css'
// import styles from './Navbar.module.css';
import styles from './NavbarStyle';
import { useState } from "react";
import TextInput from "../../atom/TextInput/TextInput";
const Navbar = ({ isAuthPage }) => {
  const [randomNavbar, setRandomNavbar] = useState(Math.floor(Math.random()*16777215).toString(16))
  const [height, setHeight] = useState(70)
  return (
    <div style={styles.navbar({ backgroundColor: randomNavbar, height })}>
      <div className={styles['title-navbar']}>
        <div className="flex flex-row">
          <div className="flex-1" > ini logo </div>
          {!isAuthPage ? (
            (
              <>
                <div className="flex-1" > ini menu </div>
                <div className="flex-1" >
                  <TextInput />
                </div>
                <div className="flex flex-row" >
                  <div className="flex1">
                    Login
                  </div>
                  <div className="flex1 ml-4">
                    Register
                  </div>
                </div>
              </>
            )
          ) : <></>}
        </div>
      </div>
    </div>
  )
}


export default Navbar;