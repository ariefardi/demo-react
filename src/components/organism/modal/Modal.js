import Modal from 'react-modal';
import TextInput from "../../atom/TextInput/TextInput";
import Button from "../../atom/Button/Button";
import React from "react";
const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
    width: 480,
    height: 420,
    textAlign: 'center'
  },
};

Modal.setAppElement('#root');


const ModalUpdate = ({
  isModalOpen,
  closeModal,
  contentLabel,
  value,
  type,
  onChange,
  placeholder,
  handleUpdate
  }) => {
  return (
    <Modal
      isOpen={isModalOpen}
      onRequestClose={closeModal}
      style={customStyles}
    >
      <h2>{contentLabel}</h2>

      <div>
        <TextInput
          value={value}
          type={type}
          onChange={onChange}
          placeholder={placeholder}
        />
      </div>
      <div>
        <Button
          title="Update"
          onClick={handleUpdate}
          customStyle={{ marginTop: 12 }}
        />
      </div>
    </Modal>
  )
}


export default ModalUpdate;