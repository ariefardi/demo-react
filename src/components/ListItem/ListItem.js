import PropTypes from 'prop-types'; // ES6
import Button from "../atom/Button/Button";
const ListItem = ({ todo, deleteTodo, setUpdateData, index }) => {
    return (
      <li>
        {' - '} { todo?.todo }
          <br/>
        <Button onClick={() => deleteTodo(todo.id)}  title="Delete"/>
        <Button onClick={() =>  setUpdateData({ id: todo.id, value: todo.todo })}  title="Update"/>
      </li>
    )
};


ListItem.propTypes = {
  deleteTodo: PropTypes.func.isRequired,
  setUpdateData: PropTypes.func.isRequired,
  index: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
}
export default ListItem;