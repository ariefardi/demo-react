import React, { Component } from 'react';
import './App.css';
import TextInput from './components/atom/TextInput/TextInput';
import Text from './components/atom/Text/Text';
import AddTodo from "./components/Addtodo/AddTodo";
import ListTodo from "./components/ListTodo/ListTodo";
import Button from './components/atom/Button/Button';
import Navbar from "./components/organism/navbar/Navbar";
import Modal from './components/organism/modal/Modal';
let dataStorage = localStorage.getItem('todos')
function generateUUID() { // Public Domain/MIT
  var d = new Date().getTime();//Timestamp
  var d2 = ((typeof performance !== 'undefined') && performance.now && (performance.now()*1000)) || 0;//Time in microseconds since page-load or 0 if unsupported
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = Math.random() * 16;//random number between 0 and 16
    if(d > 0){//Use timestamp until depleted
      r = (d + r)%16 | 0;
      d = Math.floor(d/16);
    } else {//Use microseconds since page-load if supported
      r = (d2 + r)%16 | 0;
      d2 = Math.floor(d2/16);
    }
    return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
  });
}

// class component
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: '',
      todos: JSON.parse(dataStorage) || [],
      valueUpdate: '',
      idUpdate: '',
      isModalOpen: false
    }

    this.handleChange = this.handleChange.bind(this);
    this.addTodo = this.addTodo.bind(this);
    this.handleKeyPress = this.handleKeyPress.bind(this);
    this.setUpdateData = this.setUpdateData.bind(this);
    this.handleChangeUpdate = this.handleChangeUpdate.bind(this);
    this.handleUpdateTodo = this.handleUpdateTodo.bind(this);
    this.deleteTodo = this.deleteTodo.bind(this);
    this.handleModal = this.handleModal.bind(this);
  }


  handleKeyPress = (event) => {
    if(event.key === 'Enter'){
      this.addTodo();
    }
  }

  handleModal = (value) => {
    this.setState({
      isModaOpen: value
    })
  }
  addTodo () {
    if(this.state.value.length > 3) {
      let todos = this.state.todos;
      // cara old school
      // todos.push({
      //   todo: this.state.value
      // })
      // this.setState({
      //   todos,
      //   value: ''
      // })
      // localStorage.setItem('todos', JSON.stringify(todos))

      // cara new school
      this.setState({
        todos: [...todos, { todo: this.state.value, id: generateUUID() }],
        value: ''
      })
      localStorage.setItem('todos', JSON.stringify([...todos, { todo: this.state.value, id: generateUUID() }]))
    } else {
      alert('4 huruf wey')
    }
  }

  deleteTodo (id) {
    let todos = this.state.todos;
    const filteredTodo = todos.filter((todo) => (todo.id != id))
    this.setState({
      todos: filteredTodo
    })
    localStorage.setItem('todos', JSON.stringify(filteredTodo));
  }

  handleChange(event) {
    this.setState({value: event.target.value});
  }

  setUpdateData ({ id, value }) {
    this.setState({
      valueUpdate: value,
      idUpdate: id,
      isModalOpen: true
    })
  }
  handleChangeUpdate(event) {
    this.setState({valueUpdate: event.target.value});
  }

  handleUpdateTodo () {
    const updatedData = this.state.todos.map((todo) => {
      if (todo.id === this.state.idUpdate) {
        return {
          ...todo,
          todo: this.state.valueUpdate
        }
      }
      return todo;
    })
    localStorage.setItem('todos', JSON.stringify(updatedData));
    this.setState({
      todos: updatedData,
      idUpdate: '',
      valueUpdate: '',
      isModalOpen: false
    })
  }
  render() {
    // console.log('this', this.state.todos);
    return (
        <div className="App">
          <Modal
            isModalOpen={this.state.isModalOpen}
            contentLabel="Update Todo"
            value={this.state.valueUpdate}
            disabled={!this.state.idUpdate || !this.state.valueUpdate}
            onChange={this.handleChangeUpdate}
            placeholder="Update Todo"
            handleUpdate={() => this.handleUpdateTodo()}
          />
          <div>
            <TextInput
              value={this.state.value}
              type="text"
              placeholder="Input Your Todo"
              onChange={this.handleChange}
              onKeyPress={this.handleKeyPress}
            />
          </div>
          <div style={{ marginTop: 10 }}>
            <Button
              title="add todo"
              onClick={() => this.addTodo()}
              textColor={'yellow'}
              customStyle={{
                borderWidth: 4,
                borderColor: 'black',
                backgroundColor: 'blue'
              }}
            />
          </div>
          <div>
            <ListTodo
              todos={this.state.todos}
              title="List Todo Today"
              setUpdateData={this.setUpdateData}
              deleteTodo={this.deleteTodo}
            />
          </div>
        </div>
    )
  }
}

export default App;
